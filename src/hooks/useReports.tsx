import {
  IReportListItemResponse,
  IRequestOptions,
} from "peekdata-datagateway-api-sdk";
import { useEffect, useState } from "react";

export interface useReportsProps {
  getReports: (
    userId?: string | undefined,
    requestOptions?: IRequestOptions | undefined
  ) => Promise<IReportListItemResponse[]>;
  userId?: string;
  requestOptions?: IRequestOptions;
}

const useReports = ({
  getReports,
  requestOptions,
  userId,
}: useReportsProps) => {
  const [data, setdata] = useState<IReportListItemResponse[]>([]);
  const [loading, setloading] = useState(true);
  const [error, seterror] = useState("");

  useEffect(() => {
    getReports(userId, requestOptions)
      .then((res) => {
        setdata(res);
        setloading(false);
      })
      .catch((err) => {
        seterror(err);
        setloading(false);
      });
  }, []);

  return { data, loading, error, setReports: setdata };
};

export default useReports;
