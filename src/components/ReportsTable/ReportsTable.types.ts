import { ButtonTypeMap, ExtendButtonBase, SvgIconTypeMap } from "@mui/material";
import { OverridableComponent } from "@mui/material/OverridableComponent";
import { ThemeOptions } from "@mui/material/styles/createTheme";
import {
  MaterialReactTableProps,
  MRT_Row,
  MRT_TableInstance,
} from "material-react-table";
import {
  IReportListItemResponse,
  IRequestOptions,
  ReportApi,
} from "peekdata-datagateway-api-sdk";

export interface Theme extends ThemeOptions {}

export interface ReportsTableProps {
  reportApi: ReportApi;
  userId?: string;
  requestOptions?: IRequestOptions;
  tableProps?: MaterialReactTableProps<IReportListItemResponse>;
  theme?: Theme;
  enableTitleEditing?: boolean;

  EditIcon?: OverridableComponent<SvgIconTypeMap<{}, "svg">> & {
    muiName: string;
  };
  DeleteIcon?: OverridableComponent<SvgIconTypeMap<{}, "svg">> & {
    muiName: string;
  };

  AddButton?: ExtendButtonBase<ButtonTypeMap<{}, "button">>;

  onAddReportClick?: (
    table: MRT_TableInstance<IReportListItemResponse>
  ) => void;

  onEditReportClick?: (
    report: IReportListItemResponse,
    row: MRT_Row<IReportListItemResponse>,
    table: MRT_TableInstance<IReportListItemResponse>
  ) => void;
  onDeleteReportClick?: (
    report: IReportListItemResponse,
    row: MRT_Row<IReportListItemResponse>,
    table: MRT_TableInstance<IReportListItemResponse>
  ) => void;
}
