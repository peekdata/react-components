import React from "react";
import useReports from "../../hooks/useReports";
import { ReportsTableProps } from "./ReportsTable.types";
import { Table } from "../Table";
import {
  defaultReportTableColumns,
  defaultReportTableProperties,
  defaultTheme,
} from "../../constants";
import { TableRowRenderProps } from "../Table/Table.types";
import { IReportListItemResponse } from "peekdata-datagateway-api-sdk";
import Box from "@mui/material/Box";
import TableActionButton from "../TableActionButton/TableActionButton";
import Button from "@mui/material/Button";
import Delete from "@mui/icons-material/Delete";
import Edit from "@mui/icons-material/Edit";
import ThemeProvider from "@mui/material/styles/ThemeProvider";
import createTheme from "@mui/material/styles/createTheme";
import { MRT_Row, MRT_TableInstance } from "material-react-table";

const ReportsTable: React.FC<ReportsTableProps> = ({
  reportApi,
  userId,
  requestOptions,
  enableTitleEditing,
  onAddReportClick,
  onEditReportClick,
  onDeleteReportClick,
  tableProps,
  DeleteIcon,
  EditIcon,
  AddButton,
}) => {
  const { getReports } = reportApi;

  const { data, error, loading, setReports } = useReports({
    getReports,
    userId,
    requestOptions,
  });

  const theme = createTheme(defaultTheme);

  const renderToolbarAction = ({
    table,
  }: {
    table: MRT_TableInstance<IReportListItemResponse>;
  }) => {
    if (!onAddReportClick) return null;
    if (AddButton) {
      return <AddButton onClick={() => onAddReportClick(table)}></AddButton>;
    }
    return (
      <Button variant="contained" onClick={() => onAddReportClick(table)}>
        Add report
      </Button>
    );
  };

  const renderRowActions = ({
    row,
    table,
  }: TableRowRenderProps<IReportListItemResponse>) => {
    return (
      <Box sx={{ display: "flex", gap: "1rem" }}>
        {onEditReportClick && (
          <TableActionButton
            Icon={EditIcon || Edit}
            onClick={() => onEditReportClick(row.original, row, table)}
          />
        )}
        {onDeleteReportClick && (
          <TableActionButton
            Icon={DeleteIcon || Delete}
            onClick={() => onDeleteReportClick(row.original, row, table)}
          />
        )}
      </Box>
    );
  };

  const handleTitleEdit = async (
    newTitle: string,
    row: MRT_Row<IReportListItemResponse>
  ) => {
    if (!enableTitleEditing) return;
    const originalReport = row.original;

    const updatedReport = await reportApi.updateReport(
      originalReport.id,
      { ...originalReport, name: newTitle },
      requestOptions
    );

    const updatedReports = data.map((report) =>
      report.id === originalReport.id ? updatedReport : report
    );

    setReports(updatedReports);
  };

  if (error) {
    return <div>Something went wrong</div>;
  }

  return (
    <ThemeProvider theme={theme}>
      <Table
        {...defaultReportTableProperties}
        columns={defaultReportTableColumns}
        state={{ isLoading: loading }}
        renderRowActions={renderRowActions}
        renderTopToolbarCustomActions={renderToolbarAction}
        enableEditing={enableTitleEditing}
        muiTableBodyCellEditTextFieldProps={({ row }) => ({
          onBlur: (e) => handleTitleEdit(e.target.value, row),
        })}
        {...tableProps}
        data={data}
      />
    </ThemeProvider>
  );
};

export default ReportsTable;
