import React from "react";
import { TableActionButtonProps } from "./TableActionButton.types";
import Tooltip from "@mui/material/Tooltip";
import IconButton from "@mui/material/IconButton";

const TableActionButton = <TData extends Record<string, any>>(
  props: TableActionButtonProps
) => {
  const {
    Icon,
    onClick,
    tooltipText,
    showTooltip,
    showTooltipArrow,
    tooltipPlacement,
  } = props;

  if (showTooltip) {
    return (
      <Tooltip
        arrow={showTooltipArrow}
        placement={tooltipPlacement}
        title={tooltipText}
      >
        <IconButton onClick={onClick}>
          <Icon />
        </IconButton>
      </Tooltip>
    );
  } else {
    return (
      <IconButton onClick={onClick}>
        <Icon />
      </IconButton>
    );
  }
};

export default TableActionButton;
