import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { ReportApi } from "peekdata-datagateway-api-sdk";

import { ReportsTable } from "../ReportsTable";
// import { MaterialReactTableProps } from "material-react-table";
// import { defaultReportTableProperties } from "../../constants";

export default {
  title: "Reports/ReportsTable",
  component: ReportsTable,
} as ComponentMeta<typeof ReportsTable>;

const reportApi = new ReportApi({
  baseUrl: "http://localhost:8080/v1",
});

// const overides: MaterialReactTableProps<IReportListItemResponse> = {
//   ...defaultReportTableProperties,
//   state: { isLoading: false },
//   enableFullScreenToggle: true,
// };

const Template: ComponentStory<typeof ReportsTable> = (args) => (
  <ReportsTable {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  reportApi: reportApi,
  onEditReportClick: () => console.log("Edit"),
  enableTitleEditing: true,
};
