import {
  MaterialReactTableProps,
  MRT_Cell,
  MRT_Row,
  MRT_TableInstance,
} from "material-react-table";

export interface TableRowRenderProps<TData extends Record<string, any>> {
  cell: MRT_Cell<TData>;
  row: MRT_Row<TData>;
  table: MRT_TableInstance<TData>;
}

export interface TableProps<TData extends Record<string, any>>
  extends MaterialReactTableProps<TData> {}
