import React from "react";
import { TableProps } from "./Table.types";
import MaterialReactTable from "material-react-table";

const Table = <Column extends Record<string, any>>({
  columns,
  data,
  ...props
}: TableProps<Column>) => {
  return <MaterialReactTable {...props} columns={columns} data={data} />;
};

export default Table;
