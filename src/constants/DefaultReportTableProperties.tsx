import React from "react";
import { MaterialReactTableProps } from "material-react-table";
import { IReportListItemResponse } from "peekdata-datagateway-api-sdk";

const defaultTableProperties: MaterialReactTableProps<IReportListItemResponse> =
  {
    columns: [],
    data: [],
    enableHiding: true,
    enableColumnActions: false,
    enableColumnFilters: false,
    enableDensityToggle: false,
    enableSorting: false,
    enableFullScreenToggle: false,
    enableRowActions: true,
    editingMode: "cell",
    positionActionsColumn: "last",
  };

export default defaultTableProperties;
