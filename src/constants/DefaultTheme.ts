import { ThemeOptions } from "@mui/material/styles";

const primary = "rgb(225, 52, 52)";
const secondary = "rgb(119, 25, 18)";
const primaryHover = "rgba(225, 52, 52, 0.04)";
const primarySelected = "rgba(225, 52, 52, 0.08)";
const defaultTheme: ThemeOptions = {
  palette: {
    primary: {
      main: primary,
    },
    secondary: {
      main: secondary,
    },
    action: {
      hover: primaryHover,
      selected: primarySelected,
      focus: primary,
    },
  },
};

export default defaultTheme;
