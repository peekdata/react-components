import { MRT_ColumnDef } from "material-react-table";
import { IReportListItemResponse } from "peekdata-datagateway-api-sdk";

const defaultReportTableColumns: MRT_ColumnDef<IReportListItemResponse>[] = [
  {
    header: "Report title",
    accessorFn: (row) => row.name,
  },
  {
    header: "Created",
    accessorFn: (row) => row.created,
    enableEditing: false,
  },
  {
    header: "Modified",
    accessorFn: (row) => row.modified,
    enableEditing: false,
  },
];

export default defaultReportTableColumns;
