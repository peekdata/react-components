# Component library for Peekdata API

This is a component library for Peekdata API.

## Installation

Install the component library using:

`npm install --save peekdata-react-components`

or

`yarn add peekdata-react-components`
